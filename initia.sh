#!/bin/bash

# Function to check if a command exists
exists() {
  command -v "$1" >/dev/null 2>&1
}

# Check if curl exists, install if not
if ! exists curl; then
  sudo apt update && sudo apt install curl -y < "/dev/null"
fi

# Load .bash_profile if it exists
bash_profile="$HOME/.bash_profile"
if [ -f "$bash_profile" ]; then
  . "$HOME/.bash_profile"
fi

# Wait for a moment
sleep 1 && sleep 1

# Set up variables
NODE="initia"
DAEMON_HOME="$HOME/.initia"
DAEMON_NAME="initiad"
CHAIN_ID="initiation-1"

# Backup existing directory if it exists
if [ -d "$DAEMON_HOME" ]; then
  new_folder_name="${DAEMON_HOME}_$(date +"%Y%m%d_%H%M%S")"
  mv "$DAEMON_HOME" "$new_folder_name"
fi

# Set validator name if not already set
if [ -z "$VALIDATOR" ]; then
  read -p "Enter validator name: " VALIDATOR
  echo "export VALIDATOR='${VALIDATOR}'" >> "$HOME/.bash_profile"
fi

# Source .bashrc from .bash_profile
echo 'source "$HOME/.bashrc"' >> "$HOME/.bash_profile"
source "$HOME/.bash_profile"

# Install necessary packages
sudo apt update
sudo apt install make unzip clang pkg-config lz4 libssl-dev build-essential git screen jq ncdu bsdmainutils htop -y < "/dev/null"

# Install Go
echo -e '\n\e[42mInstall Go\e[0m\n' && sleep 1
cd "$HOME"
VERSION=1.22.3
wget -O go.tar.gz "https://go.dev/dl/go${VERSION}.linux-amd64.tar.gz"
sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf go.tar.gz && rm go.tar.gz
echo 'export GOROOT=/usr/local/go' >> "$HOME/.bash_profile"
echo 'export GOPATH=$HOME/go' >> "$HOME/.bash_profile"
echo 'export GO111MODULE=on' >> "$HOME/.bash_profile"
echo 'export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin' >> "$HOME/.bash_profile" && . "$HOME/.bash_profile"
go version

# Install software
echo -e '\n\e[42mInstall software\e[0m\n' && sleep 1

cd "$HOME"
rm -rf initia
git clone https://github.com/initia-labs/initia.git
cd initia
git checkout v0.2.14
make build
sudo mv build/initiad /usr/local/bin/
source .profile

# Define peers and seeds
PEERS="d37e8d7b473c5fed671cf27d82dd50039da77917@158.220.94.116:14656,4471aa7f7bcae280f9ea91f390b455e98da9bbb0@156.67.25.14:14656,f7025d890d8fcdd481743f2cd7f706850443d42a@65.109.97.224:14656,e90cc1ef86aa7b181f947a4be7123097fff3edce@194.147.58.93:14656,fcec43c97f42a16397718f0d7139829de3911a5e@149.50.114.93:14656,f48610351be116d5e01ebee3e9c6c4178091f480@65.109.113.233:25756"
SEEDS="2eaa272622d1ba6796100ab39f58c75d458b9dbc@34.142.181.82:26656,c28827cb96c14c905b127b92065a3fb4cd77d7f6@testnet-seeds.whispernode.com:25756"

# Initialize node
"$DAEMON_NAME" init "$VALIDATOR" --chain-id "$CHAIN_ID"
sleep 1
"$DAEMON_NAME" config set client chain-id "$CHAIN_ID"
"$DAEMON_NAME" config set client keyring-backend test

# Download genesis file and configure
wget -O "$DAEMON_HOME/config/genesis.json" "https://snapshots-testnet.nodejumper.io/initia-testnet/genesis.json"
sed -i.bak -e "s|^seeds *=.*|seeds = \"${SEEDS}\"|" "$DAEMON_HOME/config/config.toml"
sed -i -e "s|^persistent_peers *=.*|persistent_peers = \"$PEERS\"|" "$DAEMON_HOME/config/config.toml"
sed -i -e "s|^minimum-gas-prices *=.*|minimum-gas-prices = \"0.15uinit,0.01uusdc\"|" "$DAEMON_HOME/config/app.toml"
sed -i -e 's|^external_address *=.*|external_address = "'"$(curl httpbin.org/ip | jq -r .origin)"':26656"|' "$DAEMON_HOME/config/config.toml"
# Configure pruning
sed -i "s|^pruning *=.*|pruning = \"custom\"|" "$DAEMON_HOME/config/app.toml"
sed -i "s|^pruning-keep-recent *=.*|pruning-keep-recent = \"100\"|" "$DAEMON_HOME/config/app.toml"
sed -i "s|^pruning-interval *=.*|pruning-interval = \"19\"|" "$DAEMON_HOME/config/app.toml"
sed -i -e "s|^indexer *=.*|indexer = \"null\"|" "$DAEMON_HOME/config/config.toml"

# Create systemd service file
cat <<EOF | sudo tee "/etc/systemd/system/$DAEMON_NAME.service" >/dev/null
[Unit]
Description=$NODE Node
After=network.target

[Service]
User=$USER
Type=simple
ExecStart=/usr/local/bin/$DAEMON_NAME start
Restart=on-failure
LimitNOFILE=65535

[Install]
WantedBy=multi-user.target
EOF

# Configure journald for persistent storage
sudo tee <<EOF >/dev/null "/etc/systemd/journald.conf"
Storage=persistent
EOF

# Download snapshot
echo -e '\n\e[42mDownloading a snapshot\e[0m\n' && sleep 1
curl -o - -L "https://files.xdrivebot.workers.dev/0:/initia-testnet_latest.tar.lz4" | lz4 -c -d - | tar -x -C "$DAEMON_HOME"
wget -O "$DAEMON_HOME/config/addrbook.json" "https://snapshots-testnet.nodejumper.io/initia-testnet/addrbook.json"

# Check and adjust ports
echo -e '\n\e[42mChecking ports\e[0m\n' && sleep 1

check_and_adjust_port() {
  local port="$1"
  if ss -tulpen | awk '{print $5}' | grep -q ":$port$" ; then
    echo -e "\e[31mPort $port already in use.\e[39m"
    sleep 2
    sed -i -e "s|:$port\"|:${port}56\"|" "$DAEMON_HOME/config/config.toml"
    echo -e "\n\e[42mPort $port changed to ${port}56.\e[0m\n"
    sleep 2
  fi
}

check_and_adjust_port 26656
check_and_adjust_port 26657
check_and_adjust_port 26658
check_and_adjust_port 6060
check_and_adjust_port 1317
check_and_adjust_port 9090
check_and_adjust_port 9091
check_and_adjust_port 8545
check_and_adjust_port 8546

# Restart systemd-journald and enable/start daemon
sudo systemctl restart systemd-journald
sudo systemctl daemon-reload
sudo systemctl enable "$DAEMON_NAME"
sudo systemctl restart "$DAEMON_NAME"

# Check node status
echo -e '\n\e[42mCheck node status\e[0m\n' && sleep 1
if sudo systemctl is-active --quiet "$DAEMON_NAME"; then
  echo -e "Your $DAEMON_NAME node \e[32minstalled and works\e[39m!"
  echo -e "You can check node status by the command \e[7msudo systemctl status $DAEMON_NAME\e[0m"
  echo -e "Press \e[7mQ\e[0m to exit from status menu"
else
  echo -e "Your $NODE node \e[31mwas not installed correctly\e[39m, please reinstall."
fi
